let notes = JSON.parse(localStorage.getItem('notes')) || [];

function addNote() {
    const noteTitle = document.getElementById('note-title').value.trim();
    const noteDesc = document.getElementById('note-desc').value.trim();

    if (noteTitle !== '' && noteDesc !== '') {
        const note = {
            title: noteTitle,
            desc: noteDesc
        };
        notes.push(note);
        localStorage.setItem('notes', JSON.stringify(notes));
        clearInputs();
        displayNotes();
    } else {
        alert('Masukkan judul dan deskrispi note!!');
    }
}

function displayNotes() {
    const noteList = document.getElementById('note-list');
    noteList.innerHTML = '';

    notes.forEach((note, index) => {
        const card = document.createElement('div');
        card.classList.add('card');
        card.innerHTML = `
          <h2>${note.title}</h2>
          <p>${note.desc}</p>
          <button class="delete-btn" onclick="deleteNote(${index})">Delete</button>
        `;
        noteList.appendChild(card);
    });

}

function deleteNote(index) {
    notes.splice(index, 1);
    localStorage.setItem('notes', JSON.stringify(notes));
    displayNotes();
}

function clearInputs() {
    document.getElementById('note-title').value = '';
    document.getElementById('note-desc').value = '';
}

displayNotes();